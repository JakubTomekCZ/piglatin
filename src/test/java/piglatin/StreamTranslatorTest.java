package piglatin;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


class StreamTranslatorTest {

    private StreamTranslator streamTranslator = new StreamTranslator();

    @Test
    void translateStreamShouldTranslateTheGivenTestFile() throws IOException {

        File translatedFile = new File("src/test/resources/HamletTranslated.txt");
        String expectedResult = Arrays.toString(Files.readAllBytes(translatedFile.toPath()));

        File inputFile = new File("src/test/resources/Hamlet.txt");
        String actualResult;
        try(FileInputStream fileInputStream = new FileInputStream(inputFile);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream()) {
            streamTranslator.translateStream(fileInputStream, byteArrayOutputStream);
            actualResult = Arrays.toString(byteArrayOutputStream.toByteArray());
        }

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void shouldThrowNullPointerExceptionWhenInputStreamIsNull() {
        assertThrows(NullPointerException.class, () -> streamTranslator.translateStream(null, System.out));
    }

    @Test
    void shouldThrowNullPointerExceptionWhenOutputStreamIsNull() {
        assertThrows(NullPointerException.class, () -> streamTranslator.translateStream(System.in, null));
    }

}