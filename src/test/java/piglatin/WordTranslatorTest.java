package piglatin;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;

class WordTranslatorTest {

    private WordTranslator wordTranslator = new WordTranslator();

    @ParameterizedTest(name = "\"{1}\" should be translated as \"{0}\"")
    @CsvSource({"\"\", \"\"",
            "?weird, ?weird",
            "Ellohay, Hello",
            "Gay, G",
            "Away, A",
            "appleway, apple",
            "aPpleway:, aPple:",
            "stairway, stairway",
            "antca’y, can’t",
            "antc!a’y, ca!n’t",
            "endway., end.",
            "histay-hingtay, this-thing",
            "histay--hingtay, this--thing",
            "Eachbay, Beach",
            "CcLoudmay, McCloud",
            "OcOn'norway, O'Connor",
            "Ocon'norway, O'coNnor",
    })
    void shouldReturnTheExpectedResult(String expectedResult, String givenWord) {
        String actualResult = wordTranslator.translateWord(givenWord);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void shouldReturnEmptyStringWhenGivenEmptyString() {
        String actualResult = wordTranslator.translateWord("");
        assertEquals("", actualResult);
    }

    @Test
    void shouldThrowNullPointerExceptionWhenGivenNull() {
        assertThrows(NullPointerException.class, () -> wordTranslator.translateWord(null));
    }

    @Test
    void shouldThrowIllegalArgumentExceptionWhenWordContainsWhitespace() {
        assertThrows(IllegalArgumentException.class, () -> wordTranslator.translateWord("translate me"));
    }

}