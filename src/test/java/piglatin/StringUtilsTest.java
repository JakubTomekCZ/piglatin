package piglatin;


import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static piglatin.StringUtils.containsWhiteSpace;
import static piglatin.StringUtils.startsWithAlphabet;
import static piglatin.StringUtils.startsWithVowel;

class StringUtilsTest {


    @ParameterizedTest
    @ValueSource(strings = {"a b", " ", "  ", "hello\n", "hello\t"})
    void doesContainWhiteSpace(String s) {
        assertTrue(containsWhiteSpace(s));
    }

    @ParameterizedTest
    @ValueSource(strings = {"hello", "", "GO!"})
    void doesNotContainWhiteSpace(String s) {
        assertFalse(containsWhiteSpace(s));
    }

    @Test
    void containsWhiteSpaceShouldThrowNullPointerExceptionWhenGivenNull() {
        assertThrows(NullPointerException.class, () -> containsWhiteSpace(null));
    }

    @ParameterizedTest
    @ValueSource(strings = {"a b", "broke", "I'm back!", "hello\n", "hello\t"})
    void doesStartWithAlphabet(String s) {
        assertTrue(startsWithAlphabet(s));
    }

    @ParameterizedTest
    @ValueSource(strings = {"- never!", "", "'o", "?weird"})
    void doesNotStartWithAlphabet(String s) {
        assertFalse(startsWithAlphabet(s));
    }

    @Test
    void startsWithAlphabetShouldThrowNullPointerExceptionWhenGivenNull() {
        assertThrows(NullPointerException.class, () -> startsWithAlphabet(null));
    }

    @ParameterizedTest
    @ValueSource(strings = {"a b", "Oh no!", "I'm back!", "Y"})
    void doesStartWithVowel(String s) {
        assertTrue(startsWithVowel(s));
    }

    @ParameterizedTest
    @ValueSource(strings = {"- never!", "", "'o", "?weird", "Help!", "cat", "dog"})
    void doesNotStartWithVowel(String s) {
        assertFalse(startsWithVowel(s));
    }

    @Test
    void startsWithVowelShouldThrowNullPointerExceptionWhenGivenNull() {
        assertThrows(NullPointerException.class, () -> startsWithVowel(null));
    }

}