package piglatin;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LineTranslatorTest {

    private LineTranslator lineTranslator = new LineTranslator();

    @Test
    void translateLineShouldReturnTranslatedLine() {
        String actualResult = lineTranslator.translateLine("‘I can,’ said     Horatio.");
        assertEquals("‘I ancay,’ aidsay Oratiohay.", actualResult);
    }

    @Test
    void shouldThrowNullPointerExceptionWhenGivenNull() {
        assertThrows(NullPointerException.class, () -> lineTranslator.translateLine(null));
    }
}