package piglatin;

class StringUtils {

    private static final String vowels = "aeiouyAEIOUY";

    // Cannot create instances
    private StringUtils() {

    }

    static boolean containsWhiteSpace(String s) {
        return !s.matches("\\S*");
    }

    static boolean startsWithAlphabet(String s) {
        return !s.isBlank() && isAlphabet(s.charAt(0));
    }

    static boolean startsWithVowel(String s) {
        return !s.isBlank() && isVowel(s.charAt(0));
    }

    static boolean isPunctuation(char c) {
        return !isAlphabet(c);
    }

    static boolean isAlphabet(char c) {
        return ((c>='a'&&c<='z')||(c>='A'&&c<='Z'));
    }

    private static boolean isVowel(char c) {
        return vowels.indexOf(c) >= 0;
    }



}
