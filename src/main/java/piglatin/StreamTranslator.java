package piglatin;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Scanner;

import static java.util.Objects.requireNonNull;

/**
 * Translates the text from the given input stream into "pig-latin" and writes it into the given output stream.
 */
class StreamTranslator {

    private LineTranslator lineTranslator = new LineTranslator();

    /**
     * Translates the text from the given input stream into "pig-latin" and writes it into the given output stream.
     * The text should contain only English alphabet letters.
     *
     * The rules for the word translation are as follows:
     *
     * <ul>
     * <li>Words that start with a consonant have their first letter moved to the end of the word and the
     * letters “ay” added to the end.
     * <ul><li>Hello becomes Ellohay</li><ul/></li>
     * <li>Words that start with a vowel have the letters “way” added to the end.
     * <ul><li>apple becomes appleway</li><ul/></li>
     * <li>Words that end with “way” are not modified. This rule overwrites the "starting letter rules".
     * <ul><li>stairway stays as stairway</li><ul/></li>
     * <li>Punctuation must remain in the same relative place from the end of the word.
     * <ul><li>can’t becomes antca’y</li>
     * <li>end. becomes endway.</li><ul/></li>
     * <li>Hyphens are treated as two words. Or more if more hyphens are present.
     * <ul><li>this-thing becomes histay-hingtay</li><ul/></li>
     * <li>Capitalization must remain in the same place. If a punctuation takes place of a capital letter capitalization
     * will be ignored.
     * <ul><li>Beach becomes Eachbay</li>
     * <li>McCloud becomes CcLoudmay</li><ul/></li>
     * <ul/>
     *
     * If no modification rule applies to the word, it will be returned in its original form.
     *
     * @param in the input stream of text to be translated into "pig-latin"
     * @param out the output stream of the translated "pig-latin" text
     * @throws NullPointerException if any of the given parameters is null
     */
    void translateStream(InputStream in, OutputStream out) throws IOException {
        requireNonNull(in);
        requireNonNull(out);
        try (Scanner scanner = new Scanner(in);
             BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(out))) {
            boolean somethingWasWritten = false;
            while(scanner.hasNextLine()) {
                String line = scanner.nextLine();
                String translatedLine = lineTranslator.translateLine(line);
                if(somethingWasWritten) {
                    bufferedWriter.newLine();
                }
                bufferedWriter.write(translatedLine);
                somethingWasWritten = true;
            }
        }
    }

}
