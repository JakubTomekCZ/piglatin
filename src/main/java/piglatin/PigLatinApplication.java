package piglatin;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * This is an example application.
 * It will translate text of a file given in argument and send it to the standard output.
 */
public class PigLatinApplication {

    public static void main(String[] args) {
        if(args.length > 0) {
            StreamTranslator streamTranslator = new StreamTranslator();
            File inputFile = new File(args[0]);
            try(FileInputStream fileInputStream = new FileInputStream(inputFile)) {
                streamTranslator.translateStream(fileInputStream, System.out);
            } catch (FileNotFoundException e) {
                System.out.println(String.format("There is no such file \"%s\".", args[0]));
            } catch (IOException e) {
                System.out.println("An unexpected error occurred. Please read the details below.");
                e.printStackTrace();
            }
        } else {
            System.out.println("There is nothing to translate. Please add a text file in a parameter.");
        }
    }
}
