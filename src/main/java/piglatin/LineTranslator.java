package piglatin;

import java.util.Scanner;

/**
 * Translates a single line into "pig-latin".
 */
class LineTranslator {

    private WordTranslator wordTranslator = new WordTranslator();

    /**
     * Translates a single line into "pig-latin".
     * The line should contain only English alphabet letters.
     *
     * The rules for the word translation are as follows:
     *
     * <ul>
     * <li>Words that start with a consonant have their first letter moved to the end of the word and the
     * letters “ay” added to the end.
     * <ul><li>Hello becomes Ellohay</li><ul/></li>
     * <li>Words that start with a vowel have the letters “way” added to the end.
     * <ul><li>apple becomes appleway</li><ul/></li>
     * <li>Words that end with “way” are not modified. This rule overwrites the "starting letter rules".
     * <ul><li>stairway stays as stairway</li><ul/></li>
     * <li>Punctuation must remain in the same relative place from the end of the word.
     * <ul><li>can’t becomes antca’y</li>
     * <li>end. becomes endway.</li><ul/></li>
     * <li>Hyphens are treated as two words. Or more if more hyphens are present.
     * <ul><li>this-thing becomes histay-hingtay</li><ul/></li>
     * <li>Capitalization must remain in the same place. If a punctuation takes place of a capital letter capitalization
     * will be ignored.
     * <ul><li>Beach becomes Eachbay</li>
     * <li>McCloud becomes CcLoudmay</li><ul/></li>
     * <ul/>
     *
     * If no modification rule applies to the word, it will be returned in its original form.
     *
     * @param line the line to be translated into "pig-latin"
     * @return the given line translated into "pig-latin"
     * @throws NullPointerException     if the given line is null
     */
    String translateLine(String line) {

        StringBuilder translatedLine = new StringBuilder();
        Scanner scanner = new Scanner(line);
        while(scanner.hasNext()) {
            String word = scanner.next();
            String translatedWord = wordTranslator.translateWord(word);
            if(translatedLine.length() > 0) {
                translatedLine.append(" ");
            }
            translatedLine.append(translatedWord);
        }
        return translatedLine.toString();
    }

}
