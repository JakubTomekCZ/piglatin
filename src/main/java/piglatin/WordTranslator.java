package piglatin;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import static piglatin.StringUtils.containsWhiteSpace;
import static piglatin.StringUtils.isAlphabet;
import static piglatin.StringUtils.isPunctuation;
import static piglatin.StringUtils.startsWithAlphabet;
import static piglatin.StringUtils.startsWithVowel;

/**
 * Translates a single word into "pig-latin".
 */
class WordTranslator {

    /**
     * Translates a single word into "pig-latin".
     * The given word can contain punctuation including hyphen but no whitespace.
     * The given word should contain only English alphabet letters.
     *
     * The rules for the word translation are as follows:
     *
     * <ul>
     * <li>Words that start with a consonant have their first letter moved to the end of the word and the
     * letters “ay” added to the end.
     * <ul><li>Hello becomes Ellohay</li><ul/></li>
     * <li>Words that start with a vowel have the letters “way” added to the end.
     * <ul><li>apple becomes appleway</li><ul/></li>
     * <li>Words that end with “way” are not modified. This rule overwrites the "starting letter rules".
     * <ul><li>stairway stays as stairway</li><ul/></li>
     * <li>Punctuation must remain in the same relative place from the end of the word.
     * <ul><li>can’t becomes antca’y</li>
     * <li>end. becomes endway.</li><ul/></li>
     * <li>Hyphens are treated as two words. Or more if more hyphens are present.
     * <ul><li>this-thing becomes histay-hingtay</li><ul/></li>
     * <li>Capitalization must remain in the same place. If a punctuation takes place of a capital letter capitalization
     * will be ignored.
     * <ul><li>Beach becomes Eachbay</li>
     * <li>McCloud becomes CcLoudmay</li><ul/></li>
     * <ul/>
     *
     * If no modification rule applies to the word, it will be returned in its original form.
     *
     * @param word the word to be translated into "pig-latin"
     * @return the given word translated into "pig-latin"
     * @throws NullPointerException     if the given word is null
     * @throws IllegalArgumentException if the given word contains a whitespace
     */
    String translateWord(String word) {

        if (containsWhiteSpace(word)) {
            throw new IllegalArgumentException("The given word contains a whitespace. Only single words are allowed.");
        }

        String[] wordsWithoutHyphens = word.split("-");
        return Arrays.stream(wordsWithoutHyphens)
                .map(this::translateWordWithoutHyphens)
                .collect(Collectors.joining("-"));
    }

    private String translateWordWithoutHyphens(String word) {
        String result = word;
        if (startsWithAlphabet(word) && !word.endsWith("way")) {

            boolean[] upperCaseLettersSnapshot = takeUpperCaseLettersSnapshot(word);
            Map<Integer, Character> punctuationSnapshot = takePunctuationSnapshot(word);

            StringBuilder transformingWord = removePunctuation(word);
            if (startsWithVowel(word)) {
                transformWordStartingWithVowel(transformingWord);
            } else {
                // starts with a consonant
                transformWordStartingWithConsonant(transformingWord);
            }

            restorePunctuationSnapshot(transformingWord, punctuationSnapshot);
            restoreUpperCaseLettersSnapshot(transformingWord, upperCaseLettersSnapshot);

            result = transformingWord.toString();
        }
        return result;
    }

    private boolean[] takeUpperCaseLettersSnapshot(String word) {
        boolean[] upperCaseLettersSnapshot = new boolean[word.length()];
        for (int i = 0; i < word.length(); i++) {
            char c = word.charAt(i);
            if (isAlphabet(c) && Character.isUpperCase(c)) {
                upperCaseLettersSnapshot[i] = true;
            }
        }
        return upperCaseLettersSnapshot;
    }

    private Map<Integer, Character> takePunctuationSnapshot(String word) {
        Map<Integer, Character> punctuationSnapshot = new LinkedHashMap<>();
        for (int i = 0; i < word.length(); i++) {
            char c = word.charAt(word.length() - i - 1);
            if (isPunctuation(c)) {
                punctuationSnapshot.put(i, c);
            }
        }
        return punctuationSnapshot;
    }

    private StringBuilder removePunctuation(String word) {
        StringBuilder transformingWord = new StringBuilder(word);
        for (int i = 0; i < transformingWord.length(); i++) {
            char c = transformingWord.charAt(i);
            if (isPunctuation(c)) {
                transformingWord.deleteCharAt(i);
                i--;
            }
        }
        return transformingWord;
    }

    private void transformWordStartingWithVowel(StringBuilder transformingWord) {
        transformingWord.append("way");
    }

    private void transformWordStartingWithConsonant(StringBuilder transformingWord) {
        char firstLetter = transformingWord.charAt(0);
        transformingWord.deleteCharAt(0)
                .append(firstLetter)
                .append("ay");
    }

    private void restorePunctuationSnapshot(StringBuilder transformingWord,
                                            Map<Integer, Character> punctuationSnapshot) {
        transformingWord.reverse();
        for (Map.Entry<Integer, Character> entry : punctuationSnapshot.entrySet()) {
            int reverseIndex = entry.getKey();
            char punctuationChar = entry.getValue();
            transformingWord.insert(reverseIndex, punctuationChar);
        }
        transformingWord.reverse();
    }

    private void restoreUpperCaseLettersSnapshot(StringBuilder transformingWord, boolean[] upperCaseLettersSnapshot) {
        for(int i = 0; i < transformingWord.length(); i++) {
            char c = transformingWord.charAt(i);
            if(isAlphabet(c)) {
                if(i < upperCaseLettersSnapshot.length && upperCaseLettersSnapshot[i]) {
                    char upperCaseChar = Character.toUpperCase(c);
                    transformingWord.deleteCharAt(i)
                        .insert(i, upperCaseChar);
                } else {
                    char lowerCaseChar = Character.toLowerCase(c);
                    transformingWord.deleteCharAt(i)
                            .insert(i, lowerCaseChar);
                }
            }
        }
    }

}
